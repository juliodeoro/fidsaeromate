<?php 
date_default_timezone_set('America/Bogota');
require_once "config.php"; 

class Modelo 
{ 
    protected $_db;

    //fecha del día
    protected $hoy;

    public function __construct() 
    { 
        $this->hoy = date("Y\-m\-d");

        $this->_db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME); 

        if ( $this->_db->connect_errno ) 
        { 
            echo "Fallo al conectar a MySQL: ". $this->_db->connect_error; 
            return;     
        } 

        $this->_db->set_charset(DB_CHARSET); 
    } 


    public function getJSONByQuery($query)
    {
        $result = $this->_db->query($query);


        if ($result->num_rows > 0){
            while($row = $result->fetch_array(MYSQLI_NUM))
            {
                $users["dataJSON"][] = $row;
            }
            $users['result'] = true;
            $users['query'] = $query;
            return $users;
        }else {
            $arrayResult['result'] = false;
            $arrayResult['query'] = $query;
            return $arrayResult;
        }
    }

    public function insertAndUpdateByQuery($query,$responseTrue = "Consulta realizada correctamente"){
        $arrResolve =[];

        if($this->_db->query($query)){
            $arrResolve["state"] = true;
            $arrResolve["query"] = $query;
            $arrResolve["response"] = $responseTrue;
        }else{
            $arrResolve["state"] = false;
            $arrResolve["query"] = $query;
            $arrResolve["response"] = $this->_db->error;
        }

        return $arrResolve;
    }
} 
?> 