<?php  
require_once "conexion.php";


class vuelosModel extends Modelo 
{     
    public function __construct() 
    {
        parent::__construct(); 
    }

    public function getAllVuelosLLegada() 
    {
        $query = "SELECT * FROM vuelo WHERE fecha_vuelo='$this->hoy' AND hr_llega != '00:00:00' ORDER BY hr_llega;";
        //$query = "SELECT * FROM vuelo WHERE  hr_llega != '00:00:00' ORDER BY hr_llega;";

        return $this->getJSONByQuery($query);
    }

    public function getAllVuelosSalida() 
    {
        //$query = "SELECT * FROM vuelo WHERE fecha_vuelo='$this->hoy' AND hr_sale != '00:00:00' ORDER BY hr_sale;";
        $query = "SELECT * FROM vuelo WHERE  hr_llega != '00:00:00' ORDER BY hr_llega;";

        return $this->getJSONByQuery($query);
    }

    public function getVuelosSalidaDia($hr_ant){
        $query = "SELECT * FROM vuelo WHERE fecha_vuelo='$this->hoy' AND hr_sale != '00:00:00' AND hr_sale > '$hr_ant' ORDER BY hr_sale;";

        return $this->getJSONByQuery($query);
    }

    public function getVuelosLLegadasDia($hr_ant){
        $query = "SELECT * FROM vuelo WHERE fecha_vuelo='$this->hoy' AND hr_llega != '00:00:00' AND hr_llega > '$hr_ant' ORDER BY hr_sale;";

        return $this->getJSONByQuery($query);
    }

    public function getAllEstados()
    {
        $query = "SELECT * FROM estado ORDER BY nom_estado;";

        return $this->getJSONByQuery($query);
    }

    public function getAllSwitch()
    {
        $query = "SELECT switch FROM control_script";

        return $this->getJSONByQuery($query);
    }

    public function getVueloByAerolinaDate($dateVuelo,$aerolinea,$numVuelo)
    {
        $result = $this->_db->query("SELECT hr_llega FROM vuelo WHERE num_vuelo='$numVuelo' AND aerolinea='$aerolinea' AND fecha_vuelo='$dateVuelo'"); 
         
        $users = $result->fetch_array(MYSQLI_NUM);
         
        return $users; 
    }

    public function getAerolineaByID($id)
    {
        $result = $this->_db->query("SELECT nom_aerolinea FROM aerolinea WHERE cod_aerolinea= '$id';"); 
         
        $users = $result->fetch_array(MYSQLI_NUM);
         
        return $users; 
    }

    public function getNombreAerolineaByID($id)
    {
        $result = $this->_db->query("SELECT nom_aerolinea FROM aerolinea WHERE cod_aerolinea='$id';"); 
         
        $users = $result->fetch_array(MYSQLI_NUM);
         
        return $users; 
    }

    public function getNombreCiudadByID($id)
    {
        $result = $this->_db->query("SELECT nom_ciudad FROM ciudad WHERE cod_ciudad='$id'"); 
         
        $users = $result->fetch_array(MYSQLI_NUM);
         
        return $users; 
    }

    public function getNombreVueloByID($id)
    {
        $result = $this->_db->query("SELECT nom_estado FROM estado WHERE cod_estado='$id';"); 
         
        $users = $result->fetch_array(MYSQLI_NUM);
         
        return $users; 
    }

    public function updateVuelo($hr_llega, $hr_sale, $cod_estado, $num_vuelo, $aerolinea, $fec_vuelo)
    {
        $query = "UPDATE vuelo SET hr_llega='$hr_llega', hr_sale='$hr_sale', estado='$cod_estado' WHERE num_vuelo='$num_vuelo' AND aerolinea='$aerolinea' AND fecha_vuelo='$fec_vuelo'";

        return  $this->insertAndUpdateByQuery($query);
    }

    public function updateSwitch($newVal)
    {
        $query = "UPDATE control_script SET switch='$newVal' ";

        return  $this->insertAndUpdateByQuery($query);
    }
} 
  ?> 