<?php
//conectar BD
require_once '../models/vuelosModels.php';

$vuelosModel = new vuelosModel();

//llamado funciones
include ('../funciones.php');
		 
//fecha del d&#237;a
$hoy = date("Y\-m\-d");

$fecha = fecha_completa( $hoy );

//consultar la hora actual y la hora hace tres horas: necesaria para mostrar los vuelos de las ultimas tres horas
$hr = hora();
$hr_ant = hora_ant();

//consultar switch		 
$est_sw = $vuelosModel->getAllSwitch();

echo"
	<html>
	<header><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
			<meta http-equiv='refresh' content='30' />
	</header>
	<body>
	<table border='0' width='410' height='40'>
	 <tbody>
		  <tr>
		   <td align='center' colspan=5><img src='../images/stories/airplane.png' width='58' height='17'><font face='arial' size='3'>&nbsp;&nbsp;AEROPUERTO INTERNACIONAL MATECA&#209;A</font></td>
		  </tr>
		  
		  <tr>
		   <td align='left' style='background-color: #4f5352' colspan=5><font color='#ffffff' face='arial' size='1'>&nbsp;$fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$hr</font></td>
		  </tr>";

if ($est_sw["dataJSON"][0] == 1)
   {
    //actualizar switch		 
	$cons = "UPDATE control_script SET switch='0' ";
	$res_sw = mysqli_query($conex,$cons);
   
	//consultar vuelos del d&#237;a - Llegada
	$res = $vuelosModel->getVuelosLLegadasDia($hr_ant);
	$num_vuelos = count( $res["dataJSON"]);

	echo"
	 </tbody>
	</table>

	<table border='0' width='410' style='background-color: #ff9900; height: 22px'>
	 <tbody>
	  <tr>
	   <td align='center' width='40'><p><strong><font color='#ffffff' face='arial' size='2'>Hora</font></strong></p></td>
	   <td align='center' width='50'><strong><font color='#ffffff' face='arial' size='2'>Vuelo</font></strong></td>
	   <td align='center' width='100'><strong><font color='#ffffff' face='arial' size='2'>Aerol&#237;nea</font></strong></td>
	   <td align='center' width='100'><strong><font color='#ffffff' face='arial' size='2'>Origen</font></strong></td>
	   <td align='center' width='120'><strong><font color='#ffffff' face='arial' size='2'>Estado</font></strong></td>
	  </tr>";

	//leer datos de vuelos de llegada
	for ($i=0; $i<$num_vuelos; $i++)
		{
		 $vuelo = mysqli_fetch_row($res);
		 //consultar aerol&#237;nea
		 $cons = "SELECT nom_aerolinea FROM aerolinea WHERE cod_aerolinea='$vuelo[1]'";
		 $res2 = mysqli_query($conex,$cons);
		 $aerolinea = mysqli_fetch_row($res2);
		 
		 //consultar ciudad de origen
		 $cons = "SELECT nom_ciudad FROM ciudad WHERE cod_ciudad='$vuelo[5]'";
		 $res2 = mysqli_query($conex,$cons);
		 $ciudad = mysqli_fetch_row($res2);
		 
		 //consultar estado del vuelo
		 $cons = "SELECT nom_estado FROM estado WHERE cod_estado='$vuelo[7]'";
		 $res2 = mysqli_query($conex,$cons);
		 $estado = mysqli_fetch_row($res2);
		 
		 //manejar el formato de la hora para retirar los segundos
		 $hr_vuelo = $vuelo[3];
		 $hr_vuelo = strtotime($hr_vuelo);
		 $hr_vuelo = date("H:i", $hr_vuelo);
		 
		 echo"
		  <tr>
		   <td align='center' width='40' style='background-color: #4f5352'><p><font color='#ffffff' face='arial' size='2'>$hr_vuelo</font></p></td>
		   <td align='center' width='50' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$vuelo[0]</font></td>
		   <td align='center' width='100' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$aerolinea[0]</font></td>
		   <td align='center' width='100' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$ciudad[0]</font></td>
		   <td align='center' width='120' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$estado[0]</font></td>
		  </tr>";	 
		}  
	echo"
	 </tbody>
	</table>
	</body>
	</html>";
   }
else
   {
    //actualizar switch		 
	$res_sw = $vuelosModel->updateSwitch(1);
	
	//consultar vuelos del d&#237;a - Llegada		 
	$cons = "SELECT * FROM vuelo WHERE fecha_vuelo='$hoy' AND hr_sale != '00:00:00' AND hr_sale > '$hr_ant' ORDER BY hr_sale";
	$res = $vuelosModel->getAllVuelosLLegada();
	echo"
	 </tbody>
	</table>

	<table border='0' width='410' style='background-color: #0a76f4; height: 22px'>
	 <tbody>
	  <tr>
	   <td align='center' width='40'><p><strong><font color='#ffffff' face='arial' size='2'>Hora</font></strong></p></td>
	   <td align='center' width='50'><strong><font color='#ffffff' face='arial' size='2'>Vuelo</font></strong></td>
	   <td align='center' width='100'><strong><font color='#ffffff' face='arial' size='2'>Aerol&#237;nea</font></strong></td>
	   <td align='center' width='100'><strong><font color='#ffffff' face='arial' size='2'>Destino</font></strong></td>
	   <td align='center' width='120'><strong><font color='#ffffff' face='arial' size='2'>Estado</font></strong></td>
	  </tr>";

	//leer datos de vuelos de llegada

	foreach($res["dataJSON"] as $vuelo)
	{
		 //consultar aerol&#237;nea
		 $aerolinea = $vuelosModel->getNombreAerolineaByID($vuelo[1]);
		 
		 //consultar ciudad de destino
		 $ciudad = $vuelosModel->getNombreAerolineaByID($vuelo[5]);
		 
		 //consultar estado del vuelo
		 $estado = $vuelosModel->getNombreVueloByID($vuelo[7]);

		 //manejar el formato de la hora para retirar los segundos
		 $hr_vuelo = $vuelo[4];
		 $hr_vuelo = strtotime($hr_vuelo);
		 $hr_vuelo = date("H:i", $hr_vuelo);
		 
		 echo"
		  <tr>
		   <td align='center' width='40' style='background-color: #4f5352'><p><font color='#ffffff' face='arial' size='2'>$hr_vuelo</font></p></td>
		   <td align='center' width='50' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$vuelo[0]</font></td>
		   <td align='center' width='100' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$aerolinea[0]</font></td>
		   <td align='center' width='100' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$ciudad[0]</font></td>
		   <td align='center' width='120' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$estado[0]</font></td>
		  </tr>";	 
		}  
	echo"
	 </tbody>
	</table>
	</body>
	</html>";   
   }

?>