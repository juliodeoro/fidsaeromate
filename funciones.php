<?php
	require_once 'models/vuelosModels.php';

	$vuelosModel = new vuelosModel();

/* funci�n para mostrar la fecha completa */
function fecha_completa( $fecha )
  {
   $seg = strtotime( $fecha ); // segundos transcurridos desde el valor de "epoch"
   $name_day = date( "l" , $seg ); // nombre del d�a en ingl�s
   $name_day = nom_dia($name_day); // conversi�n del nombre a espa�ol con la funci�n nom_dia()
   $name_month = date( "F" , $seg ); // nombre del mes en ingl�s
   $name_month = nom_mes($name_month); // conversi�n del nombre a espa�ol con la funci�n nom_mes()
   $num_dia = date( "j" , $seg ); // numero del d�a del mes
   $num_ano = date( "Y" , $seg ); // numero del a�o
   $fecha_completa = $name_day.', '.$num_dia.' de '.$name_month.' de '.$num_ano; // se estructura la fecha completa

   return $fecha_completa;
  }

/* Funci�n para convertir el nombre de los d�as del Ingl�s al Espa�ol */

function nom_dia($nom)
  {
   switch ($nom)
          {
           case "Sunday": $nom = 'Domingo';
                        break;

           case "Monday": $nom = 'Lunes';
                        break;

           case "Tuesday": $nom = 'Martes';
                        break;

           case "Wednesday": $nom = 'Mi�rcoles';
                        break;

           case "Thursday": $nom = 'Jueves';
                        break;

           case "Friday": $nom = 'Viernes';
                        break;

           case "Saturday": $nom = 'S�bado';
                        break;
  
          }
   return $nom;
  }
  
/* Funci�n para convertir el nombre del mes del Ingl�s al Espa�ol */

function nom_mes($nom)
  {
   switch ($nom)
          {
           case "January": $nom = 'Enero';
                        break;

           case "February": $nom = 'Febrero';
                        break;

           case "March": $nom = 'Marzo';
                        break;

           case "April": $nom = 'Abril';
                        break;

           case "May": $nom = 'Mayo';
                        break;

           case "June": $nom = 'Junio';
                        break;

           case "July": $nom = 'Julio';
                        break;

           case "August": $nom = 'Agosto';
                        break;

           case "September": $nom = 'Septiembre';
                        break;

           case "October": $nom = 'Octubre';
                        break;

           case "November": $nom = 'Noviembre';
                        break;

           case "December": $nom = 'Diciembre';
                        break;

           case "1" : $nom = 'Enero';
                        break;

           case "2" : $nom = 'Febrero';
                        break;

           case "3": $nom = 'Marzo';
                        break;

           case "4" : $nom = 'Abril';
                        break;

           case "5": $nom = 'Mayo';
                        break;

           case "6" : $nom = 'Junio';
                        break;

           case "7" : $nom = 'Julio';
                        break;

           case "8" : $nom = 'Agosto';
                        break;

           case "9" : $nom = 'Septiembre';
                        break;

           case "10" : $nom = 'Octubre';
                        break;

           case "11" : $nom = 'Noviembre';
                        break;

           case "12" : $nom = 'Diciembre';
                        break;
    
          }
   return $nom;
  }

/* funcion para calcular la hora, tres horas antes de la actual */

function hora_ant() 
{
  // primero se calcula la hora del momento
  $hr_act = time();
  $hr_ant = $hr_act - 3600;
  $hr_ant = date("H:i:s" , $hr_ant);

  return $hr_ant;
}

/* funcion para calcular la hora */

function hora() 
{
  $hr = time();
  $hr = date("H:i" , $hr);

  return $hr;
}


/* funci�n para mostrar en un select el estado de un vuelo */

function est_vuelo($estado) {
	echo $estado;
	$res = $vuelosModel->getAllEstados();;
	foreach ($res as $est) {
	   if ($est[0] == $estado)
	     echo "<option selected>".($estado);
	   else
	     echo "<option>".($est[0]);
	}
}

/* funci�n para mostrar en un select las aerol�neas */

function aerolinea($nom_aero) {
$cons="SELECT nom_aerolinea FROM aerolinea ORDER BY nom_aerolinea";
$bd = mysql_connect('localhost','aeroma','Cpanel2015');
mysql_select_db('aeroma_vuelos', $bd);
$res = mysql_query($cons, $bd);
while($aero = mysql_fetch_array($res))
                      {
					   if ($aero[0] == $nom_aero)
                         echo "<option selected>".($nom_aero);
					   else
					     echo "<option>".($aero[0]);
                      }
//cerrar conexion BD
mysql_close($bd);
}

					   
/* funci�n para mostrar en un select las ciudades */

function ciudad($nom_ciudad) {
$cons="SELECT nom_ciudad FROM ciudad ORDER BY nom_ciudad";
$bd = mysql_connect('localhost','aeroma','Cpanel2015');
mysql_select_db('aeroma_vuelos', $bd);
$res = mysql_query($cons, $bd);
while($ciudad = mysql_fetch_array($res))
                      {
					   if ($ciudad[0] == $nom_ciudad)
                         echo "<option selected>".($nom_ciudad);
					   else
					     echo "<option>".($ciudad[0]);
                      }
//cerrar conexion BD
mysql_close($bd);
}

/* funciones para la toma de la fecha actual */

function fecha() 
{
  // primero se calcula la diferencia horaria del momento
  $dif_hr = date("Z"); // esta funci�n devuelve un valor en segundos dependiendo el GMT
  if ($dif_hr == -14400)
    { $seg_reales  = date("U")-3600; 
      $hoy         = date("Y\-m\-d" , $seg_reales); }
  else
      $hoy = date("Y\-m\-d");

return $hoy;
}

/* funci�n para mostrar en un select una estructura de selecci�n de fechas */

  function fechas($anio, $mes, $dia) {
			
			if(empty($anio))
				{
					//extraer a�o, mes y d�a fecha actual para iniciar calendario
					$fecha = strtotime(fecha());
					$anio = date("Y",$fecha);
					$mes = date("m",$fecha);
					$dia = date("d",$fecha);				
				}

         
         echo"<font size=2 color='#336699' face='verdana'>A�o&nbsp;<select name='anio'>";
                                                                 
																 if ($anio == '2009') 
																    $a_1=selected; 
															     echo"<option value='2009' $a_1>2009</option>";
																 if ($anio == '2010') 
																    $a_2=selected; 
															     echo"<option value='2010' $a_2>2010</option>";
																 if ($anio == '2011') 
																    $a_3=selected; 
															     echo"<option value='2011' $a_3>2011</option>";
																 if ($anio == '2012') 
																    $a_4=selected; 
															     echo"<option value='2012' $a_4>2012</option>";
																 if ($anio == '2013') 
																    $a_5=selected; 
															     echo"<option value='2013' $a_5>2013</option>";
																 if ($anio == '2014') 
																    $a_6=selected; 
															     echo"<option value='2014' $a_6>2014</option>";
																 if ($anio == '2015') 
																    $a_7=selected; 
															     echo"<option value='2015' $a_7>2015</option>";
																 if ($anio == '2016') 
																    $a_8=selected; 
															     echo"<option value='2016' $a_8>2016</option>";
																 if ($anio == '2017') 
																    $a_9=selected; 
															     echo"<option value='2017' $a_9>2017</option>";
																 if ($anio == '2018') 
																    $a_10=selected; 
															     echo"<option value='2018' $a_10>2018</option>";
																 if ($anio == '2019') 
																    $a_11=selected; 
															     echo"<option value='2019' $a_11>2019</option>";
																 if ($anio == '2020') 
																    $a_12=selected; 
															     echo"<option value='2020' $a_12>2020</option>";
																 if ($anio == '2021') 
																    $a_13=selected; 
															     echo"<option value='2021' $a_13>2021</option>";
																 if ($anio == '2022') 
																    $a_14=selected; 
															     echo"<option value='2022' $a_14>2022</option>";
																 if ($anio == '2023') 
																    $a_15=selected; 
															     echo"<option value='2023' $a_15>2023</option>";
																 if ($anio == '2024') 
																    $a_16=selected; 
															     echo"<option value='2024' $a_16>2024</option>";
																 if ($anio == '2025') 
																    $a_17=selected; 
															     echo"<option value='2025' $a_17>2025</option>";
																 
														echo"</select>&nbsp;Mes&nbsp;<select name='mes'>";
                                                                 
																 if ($mes == '01') 
																    $m_1=selected; 
															     echo"<option value='01' $m_1>Enero</option>";
																 if ($mes == '02') 
																    $m_2=selected; 
															     echo"<option value='02' $m_2>Febrero</option>";
																 if ($mes == '03') 
																    $m_3=selected; 
															     echo"<option value='03' $m_3>Marzo</option>";
																 if ($mes == '04') 
																    $m_4=selected; 
															     echo"<option value='04' $m_4>Abril</option>";
																 if ($mes == '05') 
																    $m_5=selected; 
															     echo"<option value='05' $m_5>Mayo</option>";
																 if ($mes == '06') 
																    $m_6=selected; 
															     echo"<option value='06' $m_6>Junio</option>";
																 if ($mes == '07') 
																    $m_7=selected; 
															     echo"<option value='07' $m_7>Julio</option>";
																 if ($mes == '08') 
																    $m_8=selected; 
															     echo"<option value='08' $m_8>Agosto</option>";
																 if ($mes == '09') 
																    $m_9=selected; 
															     echo"<option value='09' $m_9>Septiembre</option>";
																 if ($mes == '10') 
																    $m_10=selected; 
															     echo"<option value='10' $m_10>Octubre</option>";
																 if ($mes == '11') 
																    $m_11=selected; 
															     echo"<option value='11' $m_11>Noviembre</option>";
																 if ($mes == '12') 
																    $m_12=selected; 
															     echo"<option value='12' $m_12>Diciembre</option>";
																 
														echo"</select>&nbsp;D�a&nbsp;</font><select name='dia'>";
                                                                 
																 if ($dia == '01') 
																    $d_1=selected; 
															     echo"<option value='01' $d_1>01</option>";
																 if ($dia == '02') 
																    $d_2=selected; 
															     echo"<option value='02' $d_2>02</option>";
																 if ($dia == '03') 
																    $d_3=selected; 
															     echo"<option value='03' $d_3>03</option>";
																 if ($dia == '04') 
																    $d_4=selected; 
															     echo"<option value='04' $d_4>04</option>";
																 if ($dia == '05') 
																    $d_5=selected; 
															     echo"<option value='05' $d_5>05</option>";
																 if ($dia == '06') 
																    $d_6=selected; 
															     echo"<option value='06' $d_6>06</option>";
																 if ($dia == '07') 
																    $d_7=selected; 
															     echo"<option value='07' $d_7>07</option>";
																 if ($dia == '08') 
																    $d_8=selected; 
															     echo"<option value='08' $d_8>08</option>";
																 if ($dia == '09') 
																    $d_9=selected; 
															     echo"<option value='09' $d_9>09</option>";
																 if ($dia == '10') 
																    $d_10=selected; 
															     echo"<option value='10' $d_10>10</option>";
																 if ($dia == '11') 
																    $d_11=selected; 
															     echo"<option value='11' $d_11>11</option>";
																 if ($dia == '12') 
																    $d_12=selected; 
															     echo"<option value='12' $d_12>12</option>";
																 if ($dia == '13') 
																    $d_13=selected; 
															     echo"<option value='13' $d_13>13</option>";
																 if ($dia == '14') 
																    $d_14=selected; 
															     echo"<option value='14' $d_14>14</option>";
																 if ($dia == '15') 
																    $d_15=selected; 
															     echo"<option value='15' $d_15>15</option>";
																 if ($dia == '16') 
																    $d_16=selected; 
															     echo"<option value='16' $d_16>16</option>";
																 if ($dia == '17') 
																    $d_17=selected; 
															     echo"<option value='17' $d_17>17</option>";
																 if ($dia == '18') 
																    $d_18=selected; 
															     echo"<option value='18' $d_18>18</option>";
																 if ($dia == '19') 
																    $d_19=selected; 
															     echo"<option value='19' $d_19>19</option>";
																 if ($dia == '20') 
																    $d_20=selected; 
															     echo"<option value='20' $d_20>20</option>";
																 if ($dia == '21') 
																    $d_21=selected; 
															     echo"<option value='21' $d_21>21</option>";
																 if ($dia == '22') 
																    $d_22=selected; 
															     echo"<option value='22' $d_22>22</option>";
																 if ($dia == '23') 
																    $d_23=selected; 
															     echo"<option value='23' $d_23>23</option>";
																 if ($dia == '24') 
																    $d_24=selected; 
															     echo"<option value='24' $d_24>24</option>";
																 if ($dia == '25') 
																    $d_25=selected; 
															     echo"<option value='25' $d_25>25</option>";
																 if ($dia == '26') 
																    $d_26=selected; 
															     echo"<option value='26' $d_26>26</option>";
																 if ($dia == '27') 
																    $d_27=selected; 
															     echo"<option value='27' $d_27>27</option>";
																 if ($dia == '28') 
																    $d_28=selected; 
															     echo"<option value='28' $d_28>28</option>";
																 if ($dia == '29') 
																    $d_29=selected; 
															     echo"<option value='29' $d_29>29</option>";
																 if ($dia == '30') 
																    $d_30=selected; 
															     echo"<option value='30' $d_30>30</option>";
																 if ($dia == '31') 
																    $d_31=selected; 
															     echo"<option value='31' $d_31>31</option>";
																 echo"</select>";
}					   
					   
					   
?>