<?php
require_once 'models/vuelosModels.php';

$vuelosModel = new vuelosModel();

//llamado funciones
include ('funciones.php');

//consultar la hora, hace tres horas: necesaria para mostrar los vuelos de las ultimas tres horas
$hr_ant = hora_ant();


//consultar vuelos del d�a - Llegada		 
$num_vuelos = $vuelosModel->getVuelosLLegadasDia($hr_ant);

echo"
<html>
<header><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
        <meta http-equiv='refresh' content='30' />
</header>
<body>
<table border='0' width='410' style='background-color: #ff9900; height: 22px'>
 <tbody>
  <tr>
   <td align='center' width='40'><p><strong><font color='#ffffff' face='arial' size='2'>Hora</font></strong></p></td>
   <td align='center' width='50'><strong><font color='#ffffff' face='arial' size='2'>Vuelo</font></strong></td>
   <td align='center' width='100'><strong><font color='#ffffff' face='arial' size='2'>Aerol�nea</font></strong></td>
   <td align='center' width='100'><strong><font color='#ffffff' face='arial' size='2'>Origen</font></strong></td>
   <td align='center' width='120'><strong><font color='#ffffff' face='arial' size='2'>Estado</font></strong></td>
  </tr>";

//leer datos de vuelos de llegada
foreach ($num_vuelos["dataJSON"] as $vuelo) {
	 //consultar aerol�nea
	 $aerolinea = $vuelosModel->getAerolineaByID($vuelo[1]);
	 
	 //consultar ciudad de origen
	 $ciudad = $vuelosModel->getNombreCiudadByID($vuelo[5]);
	 
	 //consultar estado del vuelo
	 $estado = $vuelosModel->getNombreVueloByID($vuelo[7]);
	 
	 //manejar el formato de la hora para retirar los segundos
	 $hr_vuelo = $vuelo[3];
	 $hr_vuelo = strtotime($hr_vuelo);
	 $hr_vuelo = date("H:i", $hr_vuelo);
	 
     echo"
	  <tr>
	   <td align='center' width='40' style='background-color: #4f5352'><p><font color='#ffffff' face='arial' size='2'>$hr_vuelo</font></p></td>
	   <td align='center' width='50' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$vuelo[0]</font></td>
	   <td align='center' width='100' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$aerolinea[0]</font></td>
	   <td align='center' width='100' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$ciudad[0]</font></td>
	   <td align='center' width='120' style='background-color: #4f5352'><font color='#ffffff' face='arial' size='2'>$estado[0]</font></td>
	  </tr>";	 
	}  
echo"
	  <tr>
	   <td align='left' style='background-color: #4f5352' colspan=5><font color='#ffffff' face='arial' size='1'>Informaci�n de las �ltimas tres horas.</font></td>
	  </tr>
 </tbody>
</table>
</body>
</html>";
?>