<?php

require_once 'models/vuelosModels.php';

$vuelosModel = new vuelosModel();

//recibir varaibles ocultas
$num_vuelo = $_POST['num_vuelo'];
$aerolinea = $_POST['aerolinea'];
$fec_vuelo = $_POST['fec_vuelo'];
$est_vuelo = $_POST['est_vuelo'];
$hora = $_POST['hora'];
$min = $_POST['min'];

//concatenar hora ingresada
$hr = $hora.':'.$min.':00';

//validar que la hora del vuelo no sea igual a 00:00:00, para evitar error en la validaci�n del script de informaci�n al p�blico
//ya que �ste utiliza la hora como diferenciador de un vuelo de llegada o salida.
if($hr == '00:00:00')
   $hr = '00:00:15';

//validar si es Llegada o Salida
//consultar vuelo		 
$hr_vuelo = $vuelosModel->getVueloByAerolinaDate($fec_vuelo,$aerolinea,$num_vuelo);

if($hr_vuelo[0] == '00:00:00')
   {
	$hr_sale = $hr;
	$hr_llega = '00:00:00';
   }
else
   {
	$hr_llega = $hr;
	$hr_sale = '00:00:00';	
   }
   
//insertar datos		 
$cons = $vuelosModel->updateVuelo($hr_llega, $hr_sale, $est_vuelo, $num_vuelo, $aerolinea, $fec_vuelo);
if($cons["state"])
   echo"<font color='' face='arial' size='3'>Vuelo actualizado exitosamente</font>";
else
   echo"<font color='red' face='arial' size='3'>Se presentaron problemas al momento de actualizar el vuelo !</font>";
?>