function Inicializar(){
    $(".alert").fadeOut("fast");

    $("#form-login").submit(function(e){
        e.preventDefault();
        var data = $(this).serialize();
        $.post("controllers/validUser.php",data,function(dataResponse){
            console.log("respuesta",dataResponse);
            if(dataResponse){
                console.log("correcto");
                window.location = "/edit_vuelos.php";
            }else{
                $(".alert").fadeIn("slow");
                $(".alert").fadeOut(2000);
            }
        },"json");
    });
}


$(document).ready(Inicializar);